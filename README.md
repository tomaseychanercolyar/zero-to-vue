## Quick Summary ##
This project shows one way to add Vue to MVC Razor pages

This came from the article [here](https://dev-squared.com/2019/03/07/vue-asp-net-core-razor-pages/)

## How do I get set up? ##

* git clone, build and run
* manually navigate to /counter to see the page that has Vue on it
